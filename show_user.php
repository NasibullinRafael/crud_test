<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/6/18
 * Time: 10:36 PM
 */

require_once 'session.php';
require_once 'Classes/DBManager.php';

$user = $db->getUser($_GET['id']);

if (!$user)
{
    $_SESSION['error_message'] = 'No such user!';
    header('location: index.php');
    exit();
}


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php require_once 'libraries.php' ?>
    <title>Document</title>
</head>
<body>
    <?php require_once 'header.php' ?>

    <div class="card mx-auto mt-3 border-dark" style="width: 30%;">
        <img  src="images/Silueta-Hombre.jpg" alt="Not found" width="200px" height="200px" style="margin: 10px auto">
        <div class="card-body">
            <h5 class="card-title"><b>ID:</b> <?php echo $user['id']; ?></h5>
            <h5 class="card-title"><b>Login:</b> <?php echo $user['login']; ?></h5>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><b>Full name:</b> <?php echo $user['name'] . " " . $user['surname']?></li>
            <li class="list-group-item"><b>Birthday:</b> <?php echo $user['datebirth']; ?></li>
            <li class="list-group-item"><b>Sex:</b> <?php echo $user['sex']?></li>
        </ul>
        <div class="card-body">
            <a href="index.php" class="card-link">Back</a>
            <a href="edit_user.php?id=<?php echo $user['id'] ?>" class="card-link">Edit</a>
            <a href="delete_user.php?id=<?php echo $user['id'] ?>" class="card-link">Delete</a>
        </div>
    </div>
</body>
</html>
