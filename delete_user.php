<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/7/18
 * Time: 5:46 AM
 */

require_once 'session.php';
require_once 'Classes/DBManager.php';

$user = $db->getUser($_GET['id']);
if ($user)
{
    if($user['login'] == $_SESSION['user']['login'])
    {
        $_SESSION['error_message'] = 'You can\'t remove yourself!';
        header('location: index.php');
        exit();
    }
}else
{
    $_SESSION['error_message'] = 'No such user!';
    header('location: index.php');
    exit();
}
$message = $db->deleteUser($user);
