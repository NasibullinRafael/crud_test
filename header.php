<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/6/18
 * Time: 9:11 PM
 */

$header_message = 'Hello, anonymous';

if (isset($_SESSION['user']))
{
    $header_message = 'Hello, ' . $_SESSION['user']['login'];
    $link = '<a class="navbar-brand" href="logout.php">Log out </a>';
}
else
{
    $link = '<a class="navbar-brand" href="login.php">Log in</a>';
}

echo '<nav class="navbar navbar-dark bg-dark">
    <span class="navbar-text">' . $header_message .
    '</span>
    <a class="navbar-brand" href="index.php">Main</a>
    <a class="navbar-brand" href="create_user.php">Create new user</a>'. $link .'</nav>';