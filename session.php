<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/8/18
 * Time: 5:51 PM
 */

session_start();

if(!isset($_SESSION['user']))
{
    $_SESSION['error'] = 'Sorry! You are not authorized. Log in please.';
    header('location: login.php');
    exit();
}
else
{
    $_SESSION['success'] = 'Hello, user!';
}