<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/8/18
 * Time: 10:50 PM
 */

session_start();

unset($_SESSION['user']);
$_SESSION['error'] = 'Goodbye!';
header('location: login.php');
exit();