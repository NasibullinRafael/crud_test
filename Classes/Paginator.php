<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/7/18
 * Time: 6:36 AM
 */

class Paginator
{
    private $page_default = 1;
    private $direction_default = 'ASC';
    private $sorts = ['id', 'login', 'name', 'surname', 'datebirth', 'sex'];

    public function getAllowablePage($requested_page, $maximum_pages)
    {
        if ($requested_page > $maximum_pages)
        {
            return $maximum_pages;

        }elseif($requested_page < 1)
        {
            return $this->page_default;
        }
        else
        {
            return $requested_page;
        }
    }

    public function getAllowableSorting($requested_sort)
    {
        if (in_array($requested_sort, $this->sorts))
        {
            return $requested_sort;
        }
        return $this->sorts[0];
    }

    public function getAllowableDirection($requested_direction)
    {
        if ($requested_direction == 'DESC')
        {
            return $requested_direction;
        }
        return $this->direction_default;
    }

}

$paginator = new Paginator;