<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/6/18
 * Time: 9:57 PM
 */

class DBManager
{

    private $pdo;
    private $limit = 10;

    public function __construct()
    {
        $dsn = 'mysql:dbname=users;host=127.0.0.1:3306';
        $user = 'root';
        $password = 'root';

        try {
            $this->pdo = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    public function countAllUsers()
    {
        $query = $this->pdo->prepare('SELECT * FROM user');
        $query->execute();
        $result = $query->fetchAll();
        $count = $query->rowCount();
        return $count;
    }

    public function getUsersByPageAndOrder($page_offset, $order = null, $direction)
    {
        $user_order = is_null($order)? 'id' : $order;
        $offset = $page_offset * $this->limit;
        $query = $this->pdo->prepare("SELECT id, login, name, surname, datebirth, sex FROM user ORDER BY {$user_order} {$direction} LIMIT :limit OFFSET :offset");
        $query->bindParam('limit', $this->limit, PDO::PARAM_INT);
        $query->bindParam('offset',$offset, PDO::PARAM_INT);

        $message = '';

        if ($result = $query->execute())
        {
            return $query->fetchAll();
        }else
        {
            $message = $query->errorInfo();
            return $message;
        }
    }

    public function getUser($id = null)
    {
        $query = $this->pdo->prepare('SELECT id, login, password, name, surname, sex, datebirth FROM user WHERE id = :id');
        $query->bindParam('id', $id);
        $query->execute();
        $result = $query->fetch();
        return $result;
    }

    public function createUser($params)
    {
        $hashed_password = password_hash($params['password'], PASSWORD_BCRYPT);
        $query = $this->pdo->prepare('INSERT INTO user (login, password, name, surname, sex, datebirth) VALUES (?,?,?,?,?,?)');

        $param_values = [
            $params['login'],
            $hashed_password,
            $params['name'],
            $params['surname'],
            $params['sex'],
            $params['datebirth'],
        ];

        if ($query->execute($param_values))
        {
            $_SESSION['success_message'] = 'New user successfully created!';
            header('location: index.php');
            exit();
        }else
        {
            $message = $query->errorInfo();
        }

        return $message;

    }

    public function updateUser($params)
    {
        $hashed_password = password_hash($params['password'], PASSWORD_BCRYPT);

        $query = $this->pdo->prepare('UPDATE user SET password=:password, name=:name, surname=:surname, sex=:sex, datebirth=:datebirth WHERE id=:id');
        $query->bindParam('id',$params['id']);
        $query->bindParam('password',$hashed_password);
        $query->bindParam('name',$params['name']);
        $query->bindParam('surname',$params['surname']);
        $query->bindParam('datebirth',$params['datebirth']);
        $query->bindParam('sex',$params['sex']);

        if ($query->execute())
        {
            $_SESSION['success_message'] = 'User successfully updated!';
            header('location: index.php');
            exit();
        }else
        {
            $message = $query->errorInfo();
        }

        return $message;
    }


    public function deleteUser($user)
    {
        $query = $this->pdo->prepare('DELETE FROM user WHERE id=:id');
        $query->bindParam('id', $user['id']);

        if ($query->execute())
        {
            $_SESSION['success_message'] = 'User successfully deleted!';
            header('Location: index.php');
            exit();
        }else
        {
            $message = $query->errorInfo();
        }

        return $message;

    }

    public function findUser($login, $password)
    {

        $query = $this->pdo->prepare("SELECT login, password FROM user WHERE login='{$login}'");

        $query->execute();
        $user = $query->fetch();

        if ($user)
        {
            if (password_verify($password, $user['password']))
            {
                return 'ok';
            }else
            {
                return 'wrong';
            }
        }
        else
        {
            return false;
        }

    }

    public function findUserValidation($login)
    {

        $query = $this->pdo->prepare("SELECT login, password FROM user WHERE login='{$login}'");

        $query->execute();
        $user = $query->fetch();

        if ($user)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

$db = new DBManager;

