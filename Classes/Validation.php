<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/9/18
 * Time: 1:22 AM
 */

require_once 'DBManager.php';

class Validation
{
    private $db_manager;
    private $errors = [];

    public function __construct()
    {
        $this->db_manager = new DBManager;
    }

    public function validateUserFields($fields, $action)
    {
        foreach ($fields as $key => $field)
        {
            if(empty($field))
            {
                $this->errors[] = "{$key} can't be empty";
            }
        }

        if (count($this->errors) == 0 && $action == 'create')
        {
            $this->validateUserUnique($fields['login']);
        }

        return $this->errors;
    }

    private function validateUserUnique($login)
    {
        $result = $this->db_manager->findUserValidation($login);

        if ($result)
        {
            $this->errors = [];
            $this->errors[] = 'User with login "' . $login . '"  already exists';
        }

    }

}

$validation = new Validation;