<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/6/18
 * Time: 8:01 PM
 */
require_once 'session.php';
require_once 'Classes/DBManager.php';
require_once 'Classes/Paginator.php';

$page_message = null;

$users_count = $db->countAllUsers();

if ($users_count > 0)
{
    $page = 1;
    $page_offset = 0;
    $sort = 'id';
    $direction = 'ASC';
    $maximum_pages = ceil($users_count/10);
    $users = [];

    if (isset($_GET['page']))
    {
        $page = $paginator->getAllowablePage($_GET['page'], $maximum_pages);
    }
    $page_offset = $page - 1;

    if (isset($_GET['sort']))
    {
        $sort = $paginator->getAllowableSorting($_GET['sort']);
    }

    if (isset($_GET['direction']))
    {
        $direction = $paginator->getAllowableDirection($_GET['direction']);
    }

    $users = $db->getUsersByPageAndOrder($page_offset, $sort, $direction);

}
else
{
    $page_message = 'There is no any users in database!';
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php require_once 'libraries.php' ?>
    <title>Document</title>
</head>
<body>
    <?php require_once 'header.php' ?>
    <p></p>
    <?php if(isset($_SESSION['success_message'])){
        echo '<div class="alert alert-success session_alert" role="alert">';
        echo $_SESSION['success_message'];
        unset($_SESSION['success_message']);
        echo  '</div>';
    } ?>
    <?php if(isset($_SESSION['error_message'])){
        echo '<div class="alert alert-warning session_alert" role="alert">';
        echo $_SESSION['error_message'];
        unset($_SESSION['error_message']);
        echo  '</div>';
    } ?>
    <div class="col-md-10 mx-auto mt-5">
        <?php if ($users_count > 0){ ?>
            <div class="row">
                <div class="col-md-4 offset-6">
                    <nav aria-label="...">
                        <ul class="pagination">
                            <?php
                            if ($maximum_pages > 1)
                            {
                                for ($i=1; $i <= $maximum_pages; $i++)
                                {
                                    if ($i == $page)
                                    {
                                        echo '<li class="page-item active"><a class="page-link" href="index.php?page=' . $i . '">' . $i.  '<span class="sr-only">(current)</span></a></li>';
                                    }
                                    else{
                                        echo '<li class="page-item"><a class="page-link" href="index.php?page=' .$i . '">'. $i .'</a></li>';
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1">
                    <p>Sort by:<br>
                        <?php
                        echo  '<a href="index.php?page=' .$page . '&direction='. $direction .'&sort=id">Id</a><br>';
                        echo  '<a href="index.php?page=' .$page . '&direction='. $direction .'&sort=login"> Login</a><br>';
                        echo  '<a href="index.php?page=' .$page . '&direction='. $direction .'&sort=name"> Name</a><br>';
                        echo  '<a href="index.php?page=' .$page . '&direction='. $direction .'&sort=surname"> Surname</a><br>';
                        echo  '<a href="index.php?page=' .$page . '&direction='. $direction .'&sort=datebirth"> Birthday</a><br>';
                        echo  '<a href="index.php?page=' .$page . '&direction='. $direction .'&sort=sex">  Sex</a>';
                        ?>
                    </p>
                </div>
                <div class="col-md-1">
                    <p>Direction by:<br>
                        <?php
                        echo  '<a href="index.php?page=' .$page . '&direction=ASC&sort='. $sort .'">ASC</a><br>';
                        echo  '<a href="index.php?page=' .$page . '&direction=DESC&sort='. $sort .'"> DESC</a><br>';
                        ?>
                    </p>
                </div>
                <div class="col-md-10">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Login</th>
                        <th scope="col">Name</th>
                        <th scope="col">Surname</th>
                        <th scope="col">Birthday</th>
                        <th scope="col">Sex</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    foreach ($users as $user)
                    {
                        echo    "<tr>
                                <td>{$user['id']}</td>
                                <td>{$user['login']}</td>
                                <td>{$user['name']}</td>
                                <td>{$user['surname']}</td>
                                <td>{$user['datebirth']}</td>
                                <td>{$user['sex']}</td>
                                <td><a href='show_user.php?id=". $user['id']. "'>show</a> | 
                                    <a href='edit_user.php?id=". $user['id']. "'>edit</a> | 
                                    <a href='delete_user.php?id=". $user['id']. "'>delete</a>
                                </td>
                                  </tr>";
                    }

                    ?>
                    </tbody>

                </table>
                </div>
            </div>

        <?php }else{ ?>
            <div class="alert alert-info" role="alert">
            <?php echo $page_message; ?>
            </div>
        <?php } ?>
    </div>
</body>
</html>
