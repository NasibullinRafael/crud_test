<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/8/18
 * Time: 3:43 PM
 */

session_start();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php require_once 'libraries.php' ?>
    <title>Document</title>
</head>
<body>
<?php require_once 'header.php' ?>
<div class="col-md-6 mx-auto mt-5">
    <form action="checklogin.php" method="post">
        <div class="form-group">
            <label for="login">Login</label>
            <input type="text" class="form-control" name="login" id="login" placeholder="Enter login">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Enter password">
        </div>
        <button type="submit" name="create_user" class="btn btn-primary">Submit</button>
    </form>
    <p></p>
    <?php if(isset($_SESSION['error'])){
        echo '<div class="alert alert-danger session_alert" role="alert">';
        echo $_SESSION['error'];
        unset($_SESSION['error']);
        echo  '</div>';
    } ?>
</div>
</body>
</html>
