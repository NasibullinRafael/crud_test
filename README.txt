Instructions:

login: admin(or any other in database)
password: admin

Settings for database connection are in file 'Classes/DBManager' in construct function
======================================================================================

About:

1. Authentication

    It is possible to use any user in database as admin. By default password for every record - "admin".
    If client is anonymous he will not be able to visit other pages except login page.

2. Views

    2.1 Index

        a) List page has a pagination with 10 objects per 1 page.
        b) By default sorting is by 'id' column, first page and ASC direction. Even if someone will change this options to wrong values application will not be failed.
        c) Errors and success messages always disappear after 3 seconds.
        d) If turn off the rules about authentication and if database empty there will be no table. Only message about emptiness of database.

    2.2 Create user

        a) Form has validation on emptiness for every field. Every error refers to field which didn't pass the validation
        b) There is also a validation of unique user.

    2.3 Edit user

        a) It's impossible to change user's login. Other fiels can be changed.
        b) Password has to be changed every time admin editing the user.
        c) Login will not be changed even if admin try to enable login input in form.
        d) If change user's id in url application will show an error.

    2.4 Show

        a) If change user's id in url application will show an error.

    2.5 Delete

        a) Authenticated person can not remove himself from database.
        b) Removing of users is realized without confirmation. Only with success message.

