<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/7/18
 * Time: 4:28 AM
 */

require_once 'session.php';
require_once 'Classes/DBManager.php';
require_once 'Classes/Validation.php';

$user = $db->getUser($_GET['id']);

if (!$user)
{
    $_SESSION['error_message'] = 'No such user!';
    header('location: index.php');
    exit();
}

$date = new DateTime($user['datebirth']);
$date_formatted = $date->format('Y-m-d');


if(isset($_POST['update_user']))
{
    $params = [
        'id' => $user['id'],
        'password' => $_POST['password'],
        'name' => $_POST['name'],
        'surname' => $_POST['surname'],
        'sex' => $_POST['sex'],
        'datebirth' => $_POST['birthday']
    ];

    $user_validate_errors = [];
    $user_validate_errors = $validation->validateUserFields($params, 'update');
    if (empty($user_validate_errors))
    {
        $message = $db->updateUser($params);
    }

}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php require_once 'libraries.php' ?>
    <title>Document</title>
</head>
<body>
<?php require_once 'header.php' ?>
<div class="row">
    <div class="col-md-6 mx-auto mt-5">
        <form action="" method="post">
            <div class="form-group">
                <label for="login">Login</label>
                <input type="text" class="form-control" name="login" id="login" disabled placeholder="Enter login" value="<?php echo $user['login']?>">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="text" class="form-control" name="password" id="password" placeholder="Enter new password">
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="password" placeholder="Enter name" value="<?php echo $user['name']?>">
            </div>
            <div class="form-group">
                <label for="name">Surname</label>
                <input type="text" class="form-control" name="surname" id="password" placeholder="Enter surname" value="<?php echo $user['surname']?>">
            </div>
            <div class="form-group">
                <label for="name">Birthday</label>
                <input type="date" class="form-control" name="birthday" id="password" placeholder="Enter birthday" value="<?php echo $date_formatted ?>">
            </div>
            <?php

            if ($user['sex'] == 'male')
            {
                echo '<div class="form-check">
                <input type="radio" name="sex" class="form-check-input" id="exampleCheck1" value="male" checked>
                <label class="form-check-label" for="exampleCheck1">male</label>
            </div>
            <div class="form-check">
                <input type="radio" name="sex" class="form-check-input" id="exampleCheck2" value="female">
                <label class="form-check-label" for="exampleCheck2">female</label>
            </div>';
            }else
            {
                echo '<div class="form-check">
                <input type="radio" name="sex" class="form-check-input" id="exampleCheck1" value="male">
                <label class="form-check-label" for="exampleCheck1">male</label>
            </div>
            <div class="form-check">
                <input type="radio" name="sex" class="form-check-input" id="exampleCheck2" value="female" checked>
                <label class="form-check-label" for="exampleCheck2">female</label>
            </div>';
            }

            ?>

            <button type="submit" name="update_user" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <div class="col-md-4">
        <?php

        if (!empty($user_validate_errors))
        {
            echo '<div id="validation_error" >';
            echo '<h5 style="color: red">Errors:</h5>';
            foreach ($user_validate_errors as $error)
            {
                echo "<p style='color: red;'>{$error}</p>";
            }

            echo '<div/>';
        }

        ?>
    </div>
</div>

</body>
</html>
