<?php
/**
 * Created by PhpStorm.
 * User: borei
 * Date: 9/8/18
 * Time: 7:51 PM
 */

session_start();
require_once 'Classes/DBManager.php';

if(isset($_SESSION['user']))
{
    $_SESSION['success_message'] = $_SESSION['user']['login'] . ', you are already authorized!';
    header('location: index.php');
    exit();
}

if(empty($_POST['login']) || empty($_POST['password'])){
    unset($_SESSION['error']);
    $_SESSION['error'] = 'Login and password are required!';
    header('location: login.php');
    exit();
}
else
{
    $user = $db->findUser($_POST['login'], $_POST['password']);

    if ($user)
    {
        if ($user == 'ok') {
            $_SESSION['user']['login'] = $_POST['login'];
            $_SESSION['success_message'] = 'Welcome, ' . $_POST['login'];
            header('location: index.php');
            exit();
        }
        else
        {
            $_SESSION['error'] = "Wrong password!";
            header('location: login.php');
            exit();
        }
    }
    else
    {
        $_SESSION['error'] = "User {$_POST['login']} not found! Try again.";
        header('location: login.php');
        exit();
    }
}